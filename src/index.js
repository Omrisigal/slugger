
export function slugger(strings) {
    if(!strings) {
        throw new Error("please enter an array of strings");
    }
    let output = "";
    for (const string of strings) {
        if(typeof string != "string"){
            throw new Error("please enter an array of strings");
        }
        output+=string+"-";
    }
    output = output.substring(0, output.length - 1);
    return output;

}