import log from "@ajar/marker";
import {slugger} from "./index.js";
let arr_of_strings_arr = [
    [""],
    ["hellow","world"],
    [],
    ["hi","my","name","is","what"]
]

arr_of_strings_arr.forEach(element => {
    log.green(element," : slugged:  "+slugger(element) );
});